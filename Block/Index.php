<?php
namespace MagentoTest\BSSHelloword\Block;

use Magento\Framework\View\Element\Template;

class Index extends \Magento\Framework\View\Element\Template
{
    protected $_userFactory;

    public function __construct(
        Template\Context $context,
        \Ngay4\Internship\Model\UsersFactory $usersFactory,
        $data = []
    )
    {
        $this->_userFactory = $usersFactory;
        parent::__construct($context, $data);
    }
}
