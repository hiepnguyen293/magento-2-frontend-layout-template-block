<?php
namespace MagentoTest\BSSHelloword\Block;
class Products extends \Magento\Framework\View\Element\Template
{
    protected $_productCollectionFactory;
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
    )
    {
        $this->_productCollectionFactory=$productCollectionFactory;
        parent::__construct($context);
    }

    public function getProductCollectionByCategories($categoryId)
    {
        $objectmanager = \Magento\Framework\App\ObjectManager::getInstance();
        $categoryFactory = $objectmanager->get('\Magento\Catalog\Model\CategoryFactory');
        $category = $categoryFactory->create()->load($categoryId);
        $categoryProducts= $category->getProductCollection()->addAttributetoSelect('*');
        return $categoryProducts;
    }
}
